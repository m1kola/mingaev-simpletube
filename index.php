<?php
header('Content-Type: text/html; charset=UTF-8');
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once (__DIR__).'/libs/app.php';

$STH = $DBH->query("SELECT * FROM `settings`");
$STH->setFetchMode(PDO::FETCH_OBJ);
while($item = $STH->fetch()) {
	$settings[$item->name] = $item->value;
}

if(isset($_GET['tag']) && !empty($_GET['tag'])) {
	$tag = ucfirst(str_replace('_',' ',$_GET['tag']));
	$total = $DBH->query("SELECT COUNT(id) as rows FROM embed WHERE `category` LIKE '%{$tag}%'")->fetch(PDO::FETCH_OBJ);
}else{
	$total = $DBH->query("SELECT COUNT(id) as rows FROM embed")->fetch(PDO::FETCH_OBJ);
}
$posts = $total->rows;
$pages  = ceil($posts / $settings['perPage']);

$data = array(
	'options' => array(
		'default'   => 1,
		'min_range' => 1,
		'max_range' => $pages
	)
);

$get_pages = isset($_GET['page']) && $_GET['page'] > 1 ? $_GET['page'] : 1;
$number = trim($get_pages);
$number = filter_var($number, FILTER_VALIDATE_INT, $data);
$range  = $settings['perPage'] * ($number - 1);

if(isset($_GET['tag']) && !empty($_GET['tag'])) {
	$tag = ucfirst(str_replace('_',' ',$_GET['tag']));
	$SQL = "SELECT * FROM `embed` WHERE `category` LIKE '%{$tag}%' ORDER BY `views` DESC";
}else{
	$SQL = "SELECT * FROM `embed` ORDER BY `views` DESC";
}

$STH = $DBH->query($SQL.' LIMIT '.$range.', '.$settings['perPage']);
$STH->setFetchMode(PDO::FETCH_OBJ);

require_once (__DIR__).'/template/header.php'; // Рисуем шапку 

if ($STH->fetchColumn() > 0) { // Проверяем есть ли контент
	require_once (__DIR__).'/template/content.php'; // Если есть рисуем контент
	require_once (__DIR__).'/template/_pagination.php'; // Рисуем пагинацию
}else{
	require_once (__DIR__).'/template/404.php'; // Если нет показываем заглушку
}

require_once (__DIR__).'/template/toplist.php'; // Рисукм футер
require_once (__DIR__).'/template/footer.php'; // Рисукм футер
?>