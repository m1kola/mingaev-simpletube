<?php
/**************************************
* Создание базы и                     *
* открытие подключения                *
**************************************/
try {
	$DBH = new PDO("sqlite:".(__DIR__)."/ntdb/".md5((__DIR__)).".db");
	$DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	checkInstall($DBH);
} catch (Exception $e) {
	echo "Хьюстон, у нас проблемы.";  
	file_put_contents((__DIR__).'/ntdb/logs/PDOErrors.txt', '['.date('d-m-Y H:i:s').'] '.$e->getMessage()."\n", FILE_APPEND);
}

/**************************************
* Функции                             *
**************************************/
function passwordGen($charCount) {
	$chars = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	$len = strlen($chars);
	$pw = '';
	
	for ($i = 0; $i < $charCount; $i++) {
		$pw .= substr($chars, rand(0, $len-1), 1);
	}
	return str_shuffle($pw);
}

function getUrl() {
	$url  = @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
	$url .= ( $_SERVER["SERVER_PORT"] != 80 ) ? ":".$_SERVER["SERVER_PORT"] : "";
	$url .= $_SERVER["REQUEST_URI"];
	return $url;
} 

function checkInstall($DBH) {
	$STH = $DBH->query("SELECT * FROM sqlite_master WHERE type='table'");
	$rowCount = $STH->fetch();

	if(!$rowCount) {
		$STH = $DBH->exec("CREATE TABLE IF NOT EXISTS embed (id VARCHAR(255), link VARCHAR(255), thumb VARCHAR(255),title VARCHAR(255),category VARCHAR(255),views INTEGER)");

		$password = passwordGen(8);

		echo '<div class="alert alert-success">Скрипт установленн. Пароль для доступа на сайт: <strong>'.$password.'</strong></div>';

		$STH = $DBH->exec("CREATE TABLE IF NOT EXISTS `settings` (name VARCHAR(255), value VARCHAR(255));INSERT INTO `settings` (`name`,`value`) VALUES ('adminPassword','".$password."');INSERT INTO `settings` (`name`,`value`) VALUES ('backgroundColor','000000');INSERT INTO `settings` (`name`,`value`) VALUES ('wrapperColor','383838');INSERT INTO `settings` (`name`,`value`) VALUES ('colorScheme','4A68C7');INSERT INTO `settings` (`name`,`value`) VALUES ('skim','60');INSERT INTO `settings` (`name`,`value`) VALUES ('textColor','FFFFFF');INSERT INTO `settings` (`name`,`value`) VALUES ('siteTitle','NEWTUBE');INSERT INTO `settings` (`name`,`value`) VALUES ('enableAdvertising','true');INSERT INTO `settings` (`name`,`value`) VALUES ('perPage','50');INSERT INTO `settings` (`name`,`value`) VALUES ('backgroundPicture','');INSERT INTO `settings` (`name`,`value`) VALUES ('scriptVersion','1.0');INSERT INTO `settings` (`name`,`value`) VALUES ('hiddenCountTag','10');");
	}
	return true;
}

function getMenu($page) {
	$menu = array(
		array('main','Главная'),
		array('import','Импорт'),
		array('settings','Настройки'),
		array('advertising','Реклама'),
		array('tag-cloud','Обновить теги'),
		array('transfer','Перенести контент'),
		array('logout','Выход')
	);

	$ret = '';

	foreach ($menu as $item) {
		$ret .= '<li><a class="'.($page == $item[0] ? 'selectMenu' : '').'" href="?page='.$item[0].'">'.$item[1].'</a></li>';
	}

	return $ret;
}
?>