<?php
if (session_status() !== PHP_SESSION_ACTIVE) {session_start();}

define('SYSPATH', $_SERVER['DOCUMENT_ROTT']);

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once (__DIR__).'/libs/app.php';

define('BASE_DIR', $_SERVER['DOCUMENT_ROOT']);

$STH = $DBH->query("SELECT * FROM `settings`");
$STH->setFetchMode(PDO::FETCH_OBJ);
while($item = $STH->fetch()) {
	$settings[$item->name] = $item->value;
}

$pages = array('main','import','settings','logout','remove','tag-cloud','transfer','advertising','shuffle','removeDuplicate','removeAll');
$page = isset($_GET['page']) && !empty($_GET['page']) ? $_GET['page'] : 'main';
$auth = isset($_SESSION['auth']) && $_SESSION['auth'] == md5(date('d-m-Y')) ? true : false;

require_once (__DIR__).'/template/admin/header.php';

if(!$auth) {
	require_once (__DIR__).'/template/admin/auth.php';
}elseif(in_array($page, $pages)) {
	require_once (__DIR__).'/template/admin/'.$page.'.php';
}

require_once (__DIR__).'/template/admin/footer.php';
?>