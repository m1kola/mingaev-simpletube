<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


require_once (__DIR__).'/libs/app.php';

$STH = $DBH->query("SELECT * FROM `settings`");
$STH->setFetchMode(PDO::FETCH_OBJ);
while($item = $STH->fetch()) {
	$settings[$item->name] = $item->value;
}

$data['id'] = isset($_GET['id']) && is_numeric($_GET['id']) ? $_GET['id'] : 0;

if($data['id'] > 0) {
	$STH = $DBH->prepare("UPDATE `embed` SET `views` = `views` + '1' WHERE `id` = :id");
	$STH->execute($data);

	$row = $DBH->query("SELECT (link) as link FROM embed WHERE `id` = '{$data['id']}'")->fetch(PDO::FETCH_OBJ);
	$link = '/ftt2/o.php?u='.$row->link.'&s='.$settings['skim'];
}else{
	$link = 'http://google.com';
}

header('Location: '.$link.'?promo=11306', 301);
?>