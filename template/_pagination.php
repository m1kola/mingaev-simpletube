<?php
$tag = isset($_GET['tag']) && !empty($_GET['tag']) ? $_GET['tag'] : '0';
?>

<div class="pagination">
		<ul>
			<?php $prev = $get_pages == 1 ? $get_pages : $get_pages - 1; ?>
			<li><a href="?<?php echo ($tag != '0' ? 'tag='.$tag.'&' : ''); ?>page=<?php echo $prev; ?>" class="inactive ">«</a></li>
			
			<?php
			if($get_pages > 1) { 
				for ($i = $get_pages-4; $i < $get_pages-1; $i++) {
					if($get_pages == ($i+1)) {
						echo '<li><span class="current">'.($i+1).'</span></li>';
					}else{
						if($i+1 > 0) {
							echo '<li><a class="inactive " href="?'.($tag != '0' ? 'tag='.$tag.'&' : '').'page='.($i+1).'">'.($i+1).'</a></li>';
						}
					}
				}
			}

			for ($i = $get_pages-1; $i < $get_pages+9; $i++) {
				if($i < $pages) {
					if($get_pages == ($i+1)) {
						echo '<li><span class="current">'.($i+1).'</span></li>';
					}else{
						echo '<li><a class="inactive " href="?'.($tag != '0' ? 'tag='.$tag.'&' : '').'page='.($i+1).'">'.($i+1).'</a></li>';
					}
				}
			}
			?>
			
			<?php $next = $get_pages == $pages ? $get_pages : $get_pages + 1; ?>
			<li><a href="?<?php echo ($tag != '0' ? 'tag='.$tag.'&' : ''); ?>page=<?php echo $next; ?>" class="inactive ">»</a></li>
		</ul>
</div>