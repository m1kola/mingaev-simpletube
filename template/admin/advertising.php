<?php defined('SYSPATH') or die('No direct script access.'); ?>
<?php
if(isset($_POST['popunder'])) {
	file_put_contents((__DIR__).'/../_popunder.html', $_POST['popunder']);
	echo '<div class="alert alert-success"><strong>Готово!</strong> Код попандера сохранен!</div>';
}
?>
<form method="POST" action="?page=advertising" accept-charset="utf-8">
	<fieldset style="padding: 10px;">
		<legend>Попандер</legend>
		<textarea style="width: 100%; height: 150px;" name="popunder"><?php echo file_get_contents((__DIR__).'/../_popunder.html'); ?></textarea>
		<br><br><button type="submit">Сохранить</button>
	</fieldset>
</form>