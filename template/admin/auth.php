<?php
if(isset($_POST['submit'])) {
	$STH = $DBH->query("SELECT * FROM `settings` WHERE `name` = 'adminPassword' AND `value` = '{$_POST['passwd']}'");
	$STH->setFetchMode(PDO::FETCH_ASSOC); 
	$count = $STH->fetch();

	if($count) {
		$_SESSION['auth'] = md5(date('d-m-Y'));
		echo '<script>document.location.href = "?page=main";</script>';
	}else{
		echo '<div class="alert alert-danger"><b>Error!</b> Incorrect password.</div>';
	}
}
?>
<form method="POST" accept-charset="utf-8">
	<input type="password" name="passwd" placeholder="Password">
	<button name="submit" type="submit">Sign In</button>
</form>