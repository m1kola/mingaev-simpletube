<!doctype html>
<html>
<head>
	<meta charset="utf8">
	<title>MingaevSimpleTUBE</title>
	<style>
	aside,footer{padding:10px}*{margin:0;padding:0}body{background:#EFCA90;font-family:monospace}.wrapper{background:#FFF;width:1004px;margin:25px auto;border-radius:0px;border:1px solid #E0E0E0}header{padding:15px 10px;border-bottom:1px solid #E0E0E0}footer{border-top:1px solid #E0E0E0;}.alert{padding:10px;margin-bottom:10px}.alert-danger{background-color:#f2dede;border-color:#ebccd1;color:#a94442}.alert-success{background-color:#dff0d8;border-color:#d6e9c6;color:#3c763d;}.logo a,.menu li a{color:#333;text-decoration:none}.item,.menu li,.pagination li{display:inline-block}.logo{float:left}.menu{float:right;padding-top:10px}.menu li a{padding:10px}.menu li a:hover{text-decoration:underline}.selectMenu{text-decoration:underline!important;color:#ccc}.clear{clear:both}fieldset td{padding:5px}.pagination a{border:1px solid #e0e0e0;color:#333;margin:3px;padding:2px 5px;text-decoration:none;}.item-select{position:absolute;right:5px;top:5px;font-size:20px!important;color:#337AB7;text-shadow:-1px 0 #000,0 1px #000,1px 0 #000,0 -1px #000}.item{cursor:pointer}.hidden{visibility:hidden;}
	</style>
	<link href="data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAzMzMQMzMzQDMzM0AzMzMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMzMzcDMzM/8zMzP/MzMzgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADMzM6AzMzOgMzMzEDMzM5AzMzP/MzMz/zMzM8AAAAAAMzMzkDMzM7AzMzMQAAAAAAAAAAAAAAAAAAAAADMzM6AzMzP/MzMz/zMzM/8zMzP/MzMz/zMzM/8zMzP/MzMz8DMzM/8zMzP/MzMzwAAAAAAAAAAAAAAAAAAAAAAzMzOgMzMz/zMzM/8zMzP/MzMz/zMzM/8zMzP/MzMz/zMzM/8zMzP/MzMz/zMzM7AAAAAAAAAAAAAAAAAAAAAAMzMzEDMzM/8zMzP/MzMz/zMzM/8zMzPAMzMzwDMzM/8zMzP/MzMz/zMzM/8zMzMQAAAAAAAAAAAzMzMQMzMzYDMzM6AzMzP/MzMz/zMzM/8zMzMwAAAAAAAAAAAzMzMwMzMz8DMzM/8zMzP/MzMzoDMzM3AzMzMQMzMzQDMzM/8zMzP/MzMz/zMzM/8zMzOwAAAAAAAAAAAAAAAAAAAAADMzM6AzMzP/MzMz/zMzM/8zMzP/MzMzQDMzM0AzMzP/MzMz/zMzM/8zMzP/MzMzoAAAAAAAAAAAAAAAAAAAAAAzMzOQMzMz/zMzM/8zMzP/MzMz/zMzM0AzMzMgMzMzgDMzM9AzMzP/MzMz/zMzM/AzMzMwAAAAAAAAAAAzMzMQMzMz8DMzM/8zMzP/MzMzwDMzM4AzMzMgAAAAAAAAAAAAAAAAMzMz8DMzM/8zMzP/MzMz8DMzM5AzMzOQMzMz8DMzM/8zMzP/MzMz/zMzMxAAAAAAAAAAAAAAAAAAAAAAMzMzgDMzM/8zMzP/MzMz/zMzM/8zMzP/MzMz/zMzM/8zMzP/MzMz/zMzM/8zMzOQAAAAAAAAAAAAAAAAAAAAADMzM9AzMzP/MzMz/zMzM/8zMzP/MzMz/zMzM/8zMzP/MzMz/zMzM/8zMzP/MzMz0AAAAAAAAAAAAAAAAAAAAAAzMzMQMzMz0DMzM8AzMzMgMzMzsDMzM/8zMzP/MzMzwDMzMyAzMzOwMzMz0DMzMxAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADMzM4AzMzP/MzMz/zMzM4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAzMzMQMzMzQDMzM0AzMzMgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//8AAP4/AADkJwAAwAMAAMADAADgBwAAw8MAAIPBAACDwQAAg8EAAOAHAADAAwAAwAMAAOQnAAD8PwAA//8AAA==" rel="icon" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="public/css/jquery.colorpicker.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<script src="http://code.jquery.com/jquery-2.2.0.min.js"></script>
</head>
<body>
<div class="wrapper">
	<header>
		<div class="logo">
			<a target="_blank" href="index.php"><h1>MingaevSimpleTUBE</h1></a>
		</div>
		<div class="menu">
		<?php if($auth) { ?>
		<ul><?php echo getMenu($page); ?></ul>
		<?php } ?>
		</div>
		<div class="clear"></div>
	</header>
	<aside>