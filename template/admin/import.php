<?php defined('SYSPATH') or die('No direct script access.'); ?>
<a href="/manage.php?page=import&type=main">Простой импорт</a> | 
<a href="/manage.php?page=import&type=hot-url">Умный импорт (по ссылке)</a> | 
<a href="/manage.php?page=import&type=hot-vid">Умный импорт (по id ролика)</a> 
<br><br><hr><br>
<?php
$type = isset($_GET['type']) && !empty($_GET['type']) ? $_GET['type'] : 'main';

switch ($type) {
	case 'main': require_once (__DIR__).'/import/main.php'; break;
	case 'hot-url': require_once (__DIR__).'/import/hot-url.php'; break;
	case 'hot-vid': require_once (__DIR__).'/import/hot-vid.php'; break;
}

?>

<style>
.hide {
	display: none;
}
</style>
<script>
$('#showInstruction').click(function() {
	if($('#instructionBlock').hasClass('hide')) {
		$('#instructionBlock').removeClass('hide');
	}else{
		$('#instructionBlock').addClass('hide');
	}
});
</script>