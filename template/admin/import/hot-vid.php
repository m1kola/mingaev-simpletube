<?php
defined('SYSPATH') or die('No direct script access.');
set_time_limit(0);

if (isset($_POST['url']) && !empty($_POST['url'])) {
	$count = 0;
	$error = '';
	$tagCloud = array();

	$page = 0;
	$pages = isset($_POST['pages']) && $_POST['pages'] > 0 ? $_POST['pages'] : 1;
	$thumbId = $_POST['url'];

	do {
		$page++;

		$json = json_decode(file_get_contents('http://st.tubecup.org/releted_phc/json.php?'.http_build_query(array(
			'page'    => $page,
			'id'      => $thumbId,
			'sort_by' => 'ctr'
		))));

		foreach ($json as $item) {
			// Проверяем ID видео
			$import['id']       = $item->video_id;
			// Проверяем название видео
			$import['title']    = $item->title;
			// Проверяем ссылку на видео
			$import['link']     = 'http://www.hclips.com/videos/'.$item->dir;
			// Проверяем теги
			$import['category'] = $item->cats_str;
			// Качаем тумбу на сервер
			$item->url = str_replace('220x165','240x180', $item->url);
			$import['thumb'] = md5($item->url.$item->screen_main.'.jpg').'.jpg';
			if(!file_exists(BASE_DIR.'/thumbs/'.$import['thumb'])) {
				@file_put_contents(BASE_DIR.'/thumbs/'.$import['thumb'], file_get_contents($item->url.$item->screen_main.'.jpg'));
			}
			// Устанавливаем количество просмотров
			$import['views'] = 0; // Ротируем сами
			// $import['views'] = $item->video_viewed // Берем ротацию hclips 

			try {
				$STH = $DBH->prepare("INSERT INTO `embed` (`id`,`link`,`thumb`,`title`,`category`,`views`) values (:id,:link,:thumb,:title,:category,:views)");
				$STH->execute($import);
				$count++;
			} catch (Exception $e) {
				$error .= '['.$import['id'].'] '.$import['title'].' импортировать не удалось.<br>';
			}

		}
	} while($page < $pages);

	if(strlen($error) == 0) {
		echo '<div class="alert alert-success"><strong>Готово!</strong> '.$count.' видеозаписей импортированно.</div>';
	}else{
		echo '<div class="alert alert-danger">'.$error.'</div>';
	}
}
?>
<p>
<strong style="cursor: pointer; text-decoration: underline;" id="showInstruction">Как импортировать контент?</strong>
<div id="instructionBlock" class="hide">
	1. На сайте <a href="http://hclips.com/" target="_blank">hclips.com</a> ищем хорошее видео.<br>
	2. Вставляем ID видео в форму ниже.<br>
	3. Выбираем количество страниц для импорта. На 1 странице примерно 20 видео.<br>
	4. ???<br>
	5. PROFIT!<br>
</div>
</p>
<br>
<form action="manage.php?page=import&type=hot-vid" method="POST" accept-charset="utf-8">
	<input style="width: 400px;" type="text" name="url" placeholder="<?php echo rand(111111,999999); ?>">
	<input type="text" name="pages" placeholder="Количество страниц" value="1">
	<button type="submit">Import</button>
</form>