<?php
defined('SYSPATH') or die('No direct script access.');
set_time_limit(0);

if (isset($_POST['url']) && !filter_var($_POST['url'], FILTER_VALIDATE_URL) === false) {
	$link = explode("\n", file_get_contents($_POST['url']));
	$count = 0;
	$error = '';
	$tagCloud = array();

	foreach ($link as $data) {
		$element = explode('|', $data);
		// Проверяем ID видео
		if(!empty($element[0])) {
			$import['id'] = $element[0];
		}
		// Проверяем название видео
		if(!empty($element[1])) {
			$import['title'] = $element[1];
		}
		// Проверяем ссылку на видео
		if(!empty($element[2])) {
			$import['link'] = $element[2];
		}
		// Проверяем теги
		if(!empty($element[3])) {
			$import['category'] = $element[3];
		}
		// Качаем тумбу на сервер
		if(!empty($element[4])) {
			if(!file_exists((__DIR__).'/../../thumbs/'.md5($element[4]).'.jpg')) {
				@file_put_contents((__DIR__).'/../../thumbs/'.md5($element[4]).'.jpg', file_get_contents($element[4]));
			}
			$import['thumb'] = md5($element[4]).'.jpg';
		}

		$import['views'] = 0;

		try {
			$STH = $DBH->prepare("INSERT INTO `embed` (`id`,`link`,`thumb`,`title`,`category`,`views`) values (:id,:link,:thumb,:title,:category,:views)");
			$STH->execute($import);
			$count = $count + 1;
		} catch (Exception $e) {
			$error .= '['.$element[0].'] '.$element[1].' импортировать не удалось.<br>';
		}
	}
	
	if(strlen($error) == 0) {
		echo '<div class="alert alert-success"><strong>Готово!</strong> '.$count.' видеозаписей импортированно.</div>';
	}else{
		echo '<div class="alert alert-danger">'.$error.'</div>';
	}
}
?>
<p>
<strong style="cursor: pointer; text-decoration: underline;" id="showInstruction">Как импортировать контент?</strong>
<div id="instructionBlock" class="hide">
	1. Регистрируемся на <a target="_blank" href="http://tubecorporate.com/signup?ref=11306">tubecorporate.com</a><br>
	2. Далее переходим по ссылке <a target="_blank" href="http://tubecorporate.com/export/feed">Export Tools > Feeds</a><br>
	3. В строке "Select site" выбираем "HClips.com"<br>
	4. И в "CSV options" настраиваем как на этой картинке: <a target="_blank" href="template/faq/faq.png">[картинка]</a><br>
	5. Теперь копируем ссылку из блока "Link to export file" и вставляем в форму ниже.<br>
	6. ???<br>
	7. PROFIT!<br>
</div>
</p>
<br>
<form action="manage.php?page=import" method="POST" accept-charset="utf-8">
	<input style="width: 400px;" type="text" name="url" placeholder="Link to export file">
	<button type="submit">Import</button>
</form>