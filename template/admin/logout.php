<?php
defined('SYSPATH') or die('No direct script access.');

foreach ($_SESSION as $key => $value) {
	unset($_SESSION[$key]);
}
session_destroy();
echo '<script>document.location.href = "?page=main";</script>';
?>