<?php
defined('SYSPATH') or die('No direct script access.');

$pos = isset($_GET['pos']) && !empty($_GET['pos']) && $_GET['pos'] > 0 ? $_GET['pos'] : 1;
$total = $DBH->query("SELECT COUNT(id) as rows FROM embed")->fetch(PDO::FETCH_OBJ);
$posts = $total->rows;
$pages  = ceil($posts / 100);

$STH = $DBH->query("SELECT * FROM `embed` LIMIT ".(($pos-1) * 100).", 100  ");
$STH->setFetchMode(PDO::FETCH_OBJ); 
?>

<form name="removeVideos" action="?page=remove" method="POST" accept-charset="utf-8">
	<input class='remove' name='remove' type="hidden">
</form>

<form name="shuffleVideos" method="GET" accept-charset="utf-8">
	<input type="hidden" name="page" value="shuffle">
</form>

<form name="removeDuplicate" method="GET" accept-charset="utf-8">
	<input type="hidden" name="page" value="removeDuplicate">
</form>

<form name="removeAll" method="GET" accept-charset="utf-8">
	<input type="hidden" name="page" value="removeAll">
</form>

<div style="float: left;">
	Всего видео: <?php echo $total->rows; ?>&nbsp;[<a style="color: #333; text-decoration: none;" href="javascript:document.shuffleVideos.submit();"><i class="fa fa-random"></i> Перемешать видео</a>]&nbsp;[<a style="color: #333; text-decoration: none;" href="javascript:document.removeDuplicate.submit();"><i class="fa fa-files-o "></i> Удалить дубли</a>]&nbsp;[<a style="color: #333; text-decoration: none;" href="javascript:document.removeAll.submit();"><i class="fa fa-reply-all"></i> Удалить все</a>]
</div>
<div style="float: right;">
	Выбранно видео: <span class="select">0</span> [<a style="color: #333; text-decoration: none;" href="javascript:document.removeVideos.submit();"><i class="fa fa-trash"></i> Удалить выбранные</a>]
</div>
<div class="clear"></div>
<hr><br>

<?php while($row = $STH->fetch()) { ?>
<div style="position: relative;" class="item" name="<?php echo $row->title; ?>" id="<?php echo $row->id; ?>">
	<a title="Удалить видео <?php echo $row->title; ?>">
		<img width="240px" height="180" src="thumbs/<?php echo $row->thumb; ?>">
		<i id="check_<?php echo $row->id; ?>" class="fa fa-check-circle item-select hidden"></i>
	</a>
</div>
<?php } ?>

<!-- Пагинация -->
<div style="text-align: center; width: 100%; border-top: 1px solid #e0e0e0; padding-top: 10px;">
	<ul class="pagination">
	<?php
	echo '<li><a href="?page=main&pos='.($pos > 1 ? $pos - 1 : 1).'">&laquo;</a></li>';

	if($pos > 1) {
		echo '<li><a href="?page=main&pos=1">1</a></li><li><a href="?page=main&pos='.$pos.'">...</a></li>';
	}

	for($i = $pos-1; $i < ($pos+20); $i++) {
		if($i < $pages) {
			echo '<li><a '.(($pos-1) == $i ? 'style="font-weight:bold;"' : '').' href="?page=main&pos='.($i+1).'">'.($i+1).'</a></li>';
		}
	}

	if($pos != $pages && $pages > 20) {
		echo '<li><a href="?page=main&pos='.$pos.'">...</a></li><li><a href="?page=main&pos='.$pages.'">'.$pages.'</a></li>';
	}

	echo '<li><a href="?page=main&pos='.($pos < $pages? $pos + 1 : $pages).'">&raquo;</a></li>';
	?>
	</ul>
</div>

<script src="http://code.jquery.com/jquery-2.2.0.min.js"></script>
<script>
$(document).ready(function(){$(".item").click(function(e){var t="#check_"+$(this).attr("id"),a=parseInt($(".select").text());$(t).hasClass("hidden")?($(t).removeClass("hidden"),$(".remove").val($(".remove").val()+$(this).attr("id")+","),$(".select").text(a+1)):($(t).addClass("hidden"),$(".remove").val($(".remove").val().replace($(this).attr("id")+",","")),0!=a?$(".select").text(a-1):$(".select").text(0))})});
</script>