<?php
defined('SYSPATH') or die('No direct script access.');

$videos = explode(',', $_POST['remove']);
foreach ($videos as $id) {
	if(filter_var($id, FILTER_VALIDATE_INT)) {
		$data['id'] = $id;
		$STH = $DBH->prepare("DELETE FROM `embed` WHERE `id` = :id");
		$STH->execute($data);
	}
}
?>

<div class="alert alert-success"><strong>Готво!</strong> <?php echo (count($videos)-1); ?> видео удалено!</div>
<script>setInterval(function() {location.href='manage.php'}, 1000);</script>