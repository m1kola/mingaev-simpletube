<?php defined('SYSPATH') or die('No direct script access.'); ?>
<?php
$error = '';
$STH = $DBH->exec("DELETE FROM `embed`");

$dir = scandir(BASE_DIR.'/thumbs/');
if (false !== $dir) {
	$images = preg_grep('/\\.(?:png|gif|jpe?g)$/', $dir);
	foreach ($images as $thumb) {
		unlink(BASE_DIR.'/thumbs/'.$thumb);
	}
}
?>
<div class="alert alert-success"><strong>Готво!</strong> Все видео и тумбы были удалены!</div>
<script>setInterval(function() {location.href='manage.php'}, 1000);</script>