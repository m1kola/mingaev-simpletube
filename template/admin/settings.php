<?php
defined('SYSPATH') or die('No direct script access.');

if(isset($_GET['deleteBackgroundPicture'])) {
	$STH = $DBH->exec("UPDATE `settings` SET `value` = '' WHERE `name` = 'backgroundPicture'");
	echo '<div class="alert alert-success"><b>Success!</b> Картинка удалена.</div>';
	echo '<script>document.location.href = "?page=settings";</script>';
}

if(isset($_POST['submit'])) {
	$options = array('adminPassword','backgroundColor','colorScheme','textColor','siteTitle','perPage','skim','wrapperColor','hiddenCountTag');

	foreach ($options as $key) {
		$data = array('value' => $_POST[$key],'name' => $key);
		$STH = $DBH->prepare("UPDATE `settings` SET `value` = :value WHERE `name` = :name");
		$STH->execute($data);
	}
	echo '<div class="alert alert-success"><b>Success!</b> Настройки сохранены.</div>';
	echo '<script>document.location.href = "?page=settings";</script>';
}

$STH = $DBH->query("SELECT * FROM `settings`");
$STH->setFetchMode(PDO::FETCH_OBJ);
while($item = $STH->fetch()) {
	$settings[$item->name] = $item->value;
}
?>
<form method="POST" action="?page=settings" enctype="multipart/form-data" accept-charset="utf-8">
	<input type="hidden" name="page" value="settings">
	<fieldset>
		<legend>Настройки админки</legend>
		<table class="settings" width="100%">
		<tr>
			<td style="width: 150px;">Пароль для входа:</td>
			<td><input name="adminPassword" value="<?php echo $settings['adminPassword']; ?>"></td>
		</tr>
		</table>
	</fieldset>
	<br>
	<fieldset>
		<legend>Настройки сайта</legend>
		<table class="settings" width="100%">
		<tr>
			<td style="width: 150px;">Название сайта:</td>
			<td><input name="siteTitle" value="<?php echo $settings['siteTitle']; ?>"></td>
		</tr>
		<tr><td colspan="2"><hr></td></tr>
		<tr>
			<td>Тумб на струнице:</td>
			<td><input name="perPage" value="<?php echo $settings['perPage']; ?>"></td>
		</tr>
		<tr><td colspan="2"><hr></td></tr>
		<tr>
			<td style="width: 150px;">Скрывать теги:</td>
			<td><input name="hiddenCountTag" value="<?php echo $settings['hiddenCountTag']; ?>"> <small style="opacity: 0.6;">скрывать теги если в них меньше <?php echo $settings['hiddenCountTag']; ?> видео. После сохранения <a href="?page=tag-cloud">обновите теги.</a></small></td>
		</tr>
		<tr><td colspan="2"><hr></td></tr>
		<tr>
			<td>Процент скима:</td>
			<td><input name="skim" value="<?php echo $settings['skim']; ?>"></td>
		</tr>
		</table>
	</fieldset>
	<br>
	<fieldset>
		<legend>Настройки шаблона</legend>
		<table class="settings" width="100%">
		<tr>
			<td style="width: 150px;">Цвет фона:</td>
			<td><input class="color" name="backgroundColor" value="<?php echo $settings['backgroundColor']; ?>" placeholder=""></td>
		</tr>
		<tr><td colspan="2"><hr></td></tr>
		<tr>
			<td>Цветовая схема:</td>
			<td><input class="color" name="colorScheme" value="<?php echo $settings['colorScheme']; ?>" placeholder=""></td>
		</tr>
		<tr><td colspan="2"><hr></td></tr>
		<tr>
			<td>Цвет контейнера:</td>
			<td><input class="color" name="wrapperColor" value="<?php echo $settings['wrapperColor']; ?>" placeholder=""></td>
		</tr>
		<tr><td colspan="2"><hr></td></tr>
		<tr>
			<td>Цвет текста:</td>
			<td><input class="color" name="textColor" value="<?php echo $settings['textColor']; ?>" placeholder=""></td>
		</tr>
		</table>
	</fieldset>
	<br><button name="submit" type="submit">Сохранить</button>
</form>

<script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
<script src="public/js/jquery.colorpicker.js"></script>
<script>
$(document).ready(function() {
	$('.color').colorpicker({
		parts:          'popup',
		showOn:         'both',
		closeOnEscape: true,
		closeOnOutside: true,
		buttonColorize: true,
		showNoneButton: true
	});
});
</script>