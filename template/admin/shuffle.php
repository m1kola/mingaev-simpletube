<?php
defined('SYSPATH') or die('No direct script access.');

$error = '';

$STH = $DBH->query("SELECT * FROM `embed`");
$STH->setFetchMode(PDO::FETCH_OBJ);
$EMBED = $STH->fetchAll();
$STH = $DBH->exec("DELETE FROM `embed`");

shuffle($EMBED);

foreach ($EMBED as $item) {
	$import['id'] = $item->id;
	$import['title'] = $item->title;
	$import['link'] = $item->link;
	$import['category'] = $item->category;
	$import['thumb'] = $item->thumb;
	$import['views'] = 0;

	try {
		$STH = $DBH->prepare("INSERT INTO `embed` (`id`,`link`,`thumb`,`title`,`category`,`views`) values (:id,:link,:thumb,:title,:category,:views)");
		$STH->execute($import);
	} catch (Exception $e) {
		$error .= '['.$element[0].'] '.$element[1].' импортировать не удалось.<br>';
	}
}

if(strlen($error) == 0) {
	echo '<div class="alert alert-success"><strong>Готво!</strong> Все видео перемешались!</div>';
}else{
	echo '<div class="alert alert-danger"><strong>Ошибка!</strong> Некоторые видео импортировать не удалось!</div>';
	echo $error;
}
?>
<script>setInterval(function() {location.href='manage.php'}, 1000);</script>