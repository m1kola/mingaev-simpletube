<?php
defined('SYSPATH') or die('No direct script access.');

$STH = $DBH->query("SELECT * FROM `settings`");
$STH->setFetchMode(PDO::FETCH_OBJ);
while($item = $STH->fetch()) {
	$settings[$item->name] = $item->value;
}

$STH = $DBH->query("SELECT * FROM `embed`");
$STH->setFetchMode(PDO::FETCH_OBJ);
$tagCloud = array();
$tagCount = array();
$str = '';

while($row = $STH->fetch()) {
	$category = explode(',', $row->category);
	
	foreach ($category as $tag) {
		if(!in_array($tag, $tagCloud)) {
			$tagCloud[] = $tag;
			$tagCount[$tag] = 0;
		}else{
			$tagCount[$tag] = $tagCount[$tag] + 1;
		}
	}
}

asort($tagCount);

foreach ($tagCount as $tag => $count) {
	if($count < $settings['hiddenCountTag']) {
		continue;
	}

	$str .= '<a href="?tag='.strtolower(str_replace(' ','_',$tag)).'">'.$tag.' ('.$count.')</a>, ';
}

file_put_contents((__DIR__).'/../tagCloud.html', substr($str, 0, -2));

echo '<div class="alert alert-success"><strong>Готово!</strong> Теги обновлены.</div>';
echo substr($str, 0, -2);
?>