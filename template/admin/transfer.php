<?php defined('SYSPATH') or die('No direct script access.'); ?>

[<a href="?page=transfer&import">Имопрт контента</a> | <a href="?page=transfer&export">Экспорт контента</a>]<br><br><hr><br>

<?php if(isset($_GET['import'])): ?>
<?php
if(isset($_POST['data']) && !empty($_POST['data'])) {
	$data = explode("\n", $_POST['data']);
	$count = 0;
	$error = '';

	foreach ($data as $string) {
		$element = explode('|', $string);

		if(!empty($element[0])) {
			$import['id'] = $element[0];
		}
		// Проверяем название видео
		if(!empty($element[1])) {
			$import['title'] = $element[1];
		}
		// Проверяем ссылку на видео
		if(!empty($element[2])) {
			$import['link'] = $element[2];
		}
		// Проверяем теги
		if(!empty($element[3])) {
			$import['category'] = $element[3];
		}
		// Качаем тумбу на сервер
		if(!empty($element[4])) {
			if(!file_exists((__DIR__).'/../../thumbs/'.md5($element[4]).'.jpg')) {
				@file_put_contents((__DIR__).'/../../thumbs/'.md5($element[4]).'.jpg', file_get_contents($element[4]));
			}
			$import['thumb'] = md5($element[4]).'.jpg';
		}

		if(isset($element[5]) && !empty($element[5])) {
			$import['views'] = $element[5];
		}else{
			$import['views'] = 0;
		}

		try {
			$STH = $DBH->prepare("INSERT INTO `embed` (`id`,`link`,`thumb`,`title`,`category`,`views`) values (:id,:link,:thumb,:title,:category,:views)");
			$STH->execute($import);
			$count = $count + 1;
		} catch (Exception $e) {
			$error .= '['.$element[0].'] '.$element[1].' импортировать не удалось.<br>';
		}
	}

	if(strlen($error) == 0) {
		echo '<div class="alert alert-success"><strong>Готово!</strong> '.$count.' видеозаписей импортированно.</div>';
	}else{
		echo '<div class="alert alert-danger">'.$error.'</div>';
	}
}
?>

<form action="?page=transfer&import" method="POST">
	<textarea name="data" style="width: 100%; height: 300px;"></textarea>
	<br><br><button type="submit">Импортировать</button>
</form>

<?php endif; ?>

<?php if(isset($_GET['export'])): ?>
<button class="shuffle">Перемешать контент</button> <button onclick="location.href = 'manage.php?page=transfer&export&viewreset';">Не учитывать показы</button><br><br>
<textarea style="width: 100%; height: 300px;">
<?php
$STH = $DBH->query("SELECT * FROM `embed`");
$STH->setFetchMode(PDO::FETCH_ASSOC); 
while ($row = $STH->fetch()) {
	$export[0] = $row['id'];
	$export[1] = $row['title'];
	$export[2] = $row['link'];
	$export[3] = $row['category'];
	$export[4] = 'http://'.$_SERVER['HTTP_HOST'].'/thumbs/'.$row['thumb'];
	$export[5] = isset($_GET['viewreset']) ? '0' : $row['views'];

	echo implode('|', $export)."\n";
}
?>
</textarea>
<script>
function shuffle(t){var e,n,o;for(o=t.length;o;o-=1)e=Math.floor(Math.random()*o),n=t[o-1],t[o-1]=t[e],t[e]=n}function explode(t,e){var n={0:""};return 2!=arguments.length||"undefined"==typeof arguments[0]||"undefined"==typeof arguments[1]?null:""===t||t===!1||null===t?!1:"function"==typeof t||"object"==typeof t||"function"==typeof e||"object"==typeof e?n:(t===!0&&(t="1"),e.toString().split(t.toString()))}$(document).ready(function(){$(".shuffle").click(function(){var t=explode("\n",$("textarea").val());shuffle(t),$("textarea").empty().val(t.join("\n"))})});
</script>
<?php endif; ?>