<!doctype html>
<html>
<head>
	<title><?php echo $settings['siteTitle']; ?></title>
	<style>
	*{margin:0;padding:0}body{background:#<?php echo $settings['backgroundColor']; ?>}.container{margin:0 auto;width:1250px;background:#<?php echo $settings['wrapperColor']; ?>}header{min-height:100px;border-bottom: 1px solid #<?php echo $settings['backgroundColor']; ?>;}.logo{font-family:Arial,Helvetica,sans-serif;color:#<?php echo $settings['textColor']; ?>;display:inline-block;float:left;height:100%;margin:10px 0 0 15px;vertical-align:middle}.logo a{text-decoration:none;color:#<?php echo $settings['textColor']; ?>}.logo span{color:#<?php echo $settings['colorScheme']; ?>}.header-tagcloud{float:right;margin:10px 10px 0 0;max-width:900px}.item a,.wm{float:left}.header-tagcloud a{color:#<?php echo $settings['colorScheme']; ?>;text-decoration:none}.header-tagcloud span{font-weight:700;color:#<?php echo $settings['textColor']; ?>}.header-tagcloud a:hover{color:#<?php echo $settings['textColor']; ?>;text-decoration:underline}.clear{clear:both}.item a{background:#<?php echo $settings['textColor']; ?>;height:240px;margin:6px;width:300px}.item a:hover{opacity:.8}.item img{height:234px;margin:2px 0 0;padding-top:1px;padding-left:4px;width:292px}.pagination a,.pagination span{border:1px solid #ccc;display:inline-block;margin:5px;padding:2px 10px}.pagination{display:inline-block;width:100%;text-align:center}.pagination li{display:inline-block}.pagination a{color:#<?php echo $settings['colorScheme']; ?>;text-decoration:none}.pagination a:hover{cursor:pointer;background:#<?php echo $settings['textColor']; ?>;opacity:.8}.pagination span{color:#<?php echo $settings['textColor']; ?>}.mingaev,.wm{padding-top:10px}footer{padding:10px;color:#<?php echo $settings['textColor']; ?>;opacity: 0.8; border-top:1px solid #<?php echo $settings['backgroundColor']; ?>}footer a{color:#<?php echo $settings['colorScheme']; ?>}.mingaev{float:right;}.alert{padding:10px;margin-bottom:10px}.alert-danger{background-color:#f2dede;border-color:#ebccd1;color:#a94442}.alert-success{background-color:#dff0d8;border-color:#d6e9c6;color:#3c763d;}
	</style>
	<link href="data:image/x-icon;base64,AAABAAEAEBAAAAEACABoBQAAFgAAACgAAAAQAAAAIAAAAAEACAAAAAAAAAEAAAAAAAAAAAAAAAEAAAAAAAAiH+YAJCDkACYh4gAiHOwAJR7oAP/99wAkGvAAJhzsACQZ8gAmGvAAJhnyAP//+QAvHOwAJBzrAP/8+AApIOMAJBvtAG5o6AD+//8AKBvtACca7wD///8A/v/4ABkd7gD+/+oA///+AP/7/wD6+vMAKSXrAOnt/wD3/foAIh/pAP/+/wD///YA/v/oABkY9gD///wA/f/1AObp/wD7/+4AJB3sACwa7AD///sA+vP/AP//9AD+//oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFRUlFhYWFhYWFhYWFiIVFRUVBxAQEBAQEBAQEBAKFRUVFQcQEBAQHSYQEBAQChUVFRUHEBAQJxkVGBAQEAoVFRUVBxAjFRUVFRUVDBAKFRUVFQcQFRUVFRUVFRUQChUVFRUHEBUVFRUVFRUVEAoVFRUVBxAZFRUOLRUVIRAKFRUVFQcQHxYWBAEWFigQChUVGhcGEBAQEBAQEBAQEAkcFRUSKRAQEBAQEBAQEA0rFRUVFSAREBAQEBAQEBADFRUVFRUVFRsCEBAQEAALAxUVFRUVFRUVHhQQEA8gFQgVFRUVFRUVFRUFEwQsFRUVFRUVFRUVFRUVFSokFRUVFRUVFQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA=" rel="icon" type="image/x-icon" />
	<?php
	if(file_exists($_SERVER['DOCUMENT_ROOT'].'/ftt2/in.php')) {
		require_once $_SERVER['DOCUMENT_ROOT'].'/ftt2/in.php';
	}
	?>
</head>
<body>
<div class="container">
	<header>
		<div class="logo">
			<a href="/"><h1><?php echo strtoupper(str_replace('TUBE','<span>TUBE</span>', $settings['siteTitle'])); ?></h1></a>
		</div>
		<div class="header-tagcloud">
		<?php
		if(file_exists((__DIR__).'/tagCloud.html')) {
			echo '<span>TAGS: </span>';
			require_once (__DIR__).'/tagCloud.html';
		}
		?>
		</div>
		<div class="clear"></div>
		<br>
	</header>
	<aside>