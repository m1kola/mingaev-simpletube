<?php if(file_exists((__DIR__).'/../ftt2/toplists/toplist_example.php.html')): ?>
<style>
	.toplist{padding:10px;margin-top:10px;border-top:1px solid #<?php echo $settings['backgroundColor'];?>}.toplist a{color:#<?php echo $settings['textColor'];?>;display:inline-block;text-align:left;width:205px;padding:5px 0}.toplist a:hover{color:#<?php echo $settings['colorScheme'];?>}.toplist h3{padding-bottom:5px;color:#<?php echo $settings['textColor'];?>}.toplist li{float:left;list-style:none}.toplist li:nth-child(6n+1){clear:both;}
</style>
<div class="toplist">
	<h3>More Free Sites:</h3>
	<ul>
		<?php require_once (__DIR__).'/../ftt2/toplists/toplist_example.php.html'; ?>
	</ul>
</div>
<div class="clear"></div>
<?php endif; ?>